import { callApi } from '../helpers/apiHelper';
import { Details } from './../modelsAdded/details';
import { Fighter } from '../modelsAdded/fighter';

export async function getFighters():Promise<Fighter[]> {
  try {
    const endpoint = 'fighters.json';
    const apiResult = await callApi(endpoint, 'GET');

    return apiResult as Fighter[];

  } catch (error) {
    throw error;
  }
}

export async function getFighterDetails(id:number):Promise<Details> {
  // endpoint - `details/fighter/${id}.json`;
  
  try {
    const endpoint = `details/fighter/${id}.json`;
    const apiResult = await callApi(endpoint, 'GET');
    return apiResult as Details;
  } catch (error) {
    throw error;
  }
}


