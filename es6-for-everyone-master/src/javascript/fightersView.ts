import { Details } from './modelsAdded/details';
import { Fighter } from './modelsAdded/fighter';
import { createFighter } from './fighterView';
import { showFighterDetailsModal } from './modals/fighterDetails';
import { createElement } from './helpers/domHelper';
import { fight } from './fight';
import { showWinnerModal } from './modals/winner';
import { getFighterDetails } from './services/fightersService';


export function createFighters(fighters: Fighter[]) {
  const selectFighterForBattle = createFightersSelector();
  const fighterElements = fighters.map(fighter => createFighter(fighter, showFighterDetails, selectFighterForBattle));
  const fightersContainer = createElement({ tagName: 'div', className: 'fighters' });

  fightersContainer.append(...fighterElements);

  return fightersContainer;
}

const fightersDetailsCache = new Map<number, Details>();

async function showFighterDetails(event: MouseEvent, fighter: Fighter) {
  const fullInfo = await getFighterInfo(fighter._id);
  showFighterDetailsModal(fullInfo);
}

export async function getFighterInfo(fighterId: number): Promise<Details> {
  // get fighter form fightersDetailsCache or use getFighterDetails function
  if (fighterId in fightersDetailsCache.keys())
    return fightersDetailsCache.get(fighterId)!;
  const details = await getFighterDetails(fighterId);
  fightersDetailsCache.set(fighterId, details);
  return details;
}
function createFightersSelector() {
  const selectedFighters = new Map<number, Fighter>();

  return async function selectFighterForBattle(event: any, fighter: Fighter) {
    const fullInfo = await getFighterInfo(fighter._id);

    if (event.target.checked) {
      selectedFighters.set(fighter._id, fullInfo);
    } else {
      selectedFighters.delete(fighter._id);
    }

    if (selectedFighters.size === 2) {
      const [fighter1, fighter2] = selectedFighters.values();
      const promise = fight(fighter1, fighter2);
      promise.then(winner => {
        const allModals = document.getElementsByClassName('modal-layer');
        const modal = allModals[allModals.length - 1];
        modal?.remove();
        showWinnerModal(winner);
      });

    }
  }
}


