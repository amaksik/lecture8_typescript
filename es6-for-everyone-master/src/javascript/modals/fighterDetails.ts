import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
import { Fighter } from './../modelsAdded/fighter';
import { getFighterDetails } from '../services/fightersService';

export async function showFighterDetailsModal(fighter: Fighter) {
  const title = 'Fighter info';
  const bodyElement = await createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

async function createFighterDetails(fighter: Fighter): Promise<HTMLElement> {
  const { name } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  const { attack, defense, health, source: image } = await getFighterDetails(fighter._id);
  const attackStatElement = createElement({ tagName: 'div', className: 'fighter-attack' });
  const defenseStatElement = createElement({ tagName: 'div', className: 'fighter-defense' });
  const healthStatElement = createElement({ tagName: 'div', className: 'fighter-health' });
  const imageStatElement = createElement({ tagName: 'img', className: 'fighter-image', attributes: { src: image } });
  // show fighter name, attack, defense, health, image

  nameElement.innerText = `Name: ${name} 👑`;
  attackStatElement.innerText = `Attack: ${attack} 🗡️`;
  defenseStatElement.innerText = `Defense:${defense} 🛡️`;
  healthStatElement.innerText = `Health:${health} ❤️`;
  fighterDetails.append(nameElement, attackStatElement, defenseStatElement, healthStatElement, imageStatElement);
  return fighterDetails;

  return fighterDetails;
}
