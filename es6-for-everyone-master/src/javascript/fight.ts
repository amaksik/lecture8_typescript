import { Fighter } from './modelsAdded/fighter';
import { getFighterDetails } from './services/fightersService';



export async function fight(firstFighter:Fighter, secondFighter:Fighter): Promise<Fighter> {
// return winner

let winner:Fighter;

  let healthFighter1 = (await getFighterDetails(firstFighter._id)).health;
  let healthFighter2 = (await getFighterDetails(secondFighter._id)).health;
  
  while(healthFighter1 > 0 && healthFighter2>0){
    healthFighter2 -= await getDamage(firstFighter, secondFighter);
    healthFighter1 -= await getDamage(secondFighter, firstFighter);
  }

  return healthFighter2 > healthFighter1 ? secondFighter: firstFighter;
}


export async function getDamage(attacker:Fighter, enemy:Fighter) {
  // damage = hit - block
  // return damage 
  
  const damage = await getHitPower(attacker) - await getBlockPower(enemy);
  
  return damage;
}

export async function getHitPower(fighter:Fighter) {
  // return hit power
  
  const details = await getFighterDetails(fighter._id);
  const chanceOfCriticalHit:number = Math.random()*2+1;
  return details.attack*chanceOfCriticalHit;
}


export async function getBlockPower(fighter:Fighter) {
// return block power

  const details = await getFighterDetails(fighter._id);
  const chanceOfDodge:number = Math.random()*2+1;
  return details.defense*chanceOfDodge;
}

