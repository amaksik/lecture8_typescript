import { createElement } from '../helpers/domHelper';
import { getFighterDetails } from '../services/fightersService';
import { Fighter } from './../models/fighter';
import { showModal } from './modal';
export async  function showWinnerModal(fighter:Fighter) {
  const title = 'Winner info';
  const bodyElement = await createWinnerDetails(fighter)
  showModal({ title, bodyElement });
}
async function createWinnerDetails(fighter:Fighter):Promise<HTMLElement> {
  const { name } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  const { attack, defense, health, source: image } = await getFighterDetails(fighter._id);
  const attackStatElement = createElement({ tagName: 'div', className: 'fighter-attack' });
  const defenseStatElement = createElement({ tagName: 'div', className: 'fighter-defense' });
  const healthStatElement = createElement({ tagName: 'div', className: 'fighter-health' });
  const imageStatElement = createElement({ tagName: 'img', className: 'fighter-image', attributes: { src: image } });

  nameElement.innerText = `Name 🚀: ${name}`;
  attackStatElement.innerText = `Attack ⚔️: ${attack}`;
  defenseStatElement.innerText = `Defense 🛡:${defense}`;
  healthStatElement.innerText = `Health 💯:${health}`;
  fighterDetails.append(nameElement, attackStatElement, defenseStatElement, healthStatElement, imageStatElement);
  return fighterDetails;
}
